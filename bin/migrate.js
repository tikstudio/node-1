import { Groups, Messages, Users, UsersGroupRelation } from "../models";


async function main() {
  try {
    for (const Model of [
      Users,
      Groups,
      Messages,
      UsersGroupRelation
    ]) {
      console.log(Model)
      await Model.sync({ alter: true })
    }
  } catch (e) {
    console.log(e)
  }

  process.exit(0);
}

main();
