import jwt from "jsonwebtoken";
import HttpErrors from "http-errors";

const EXCLUDE = [
  '/people/check'
];
const { JWT_SECRET } = process.env

export default function authorization(req, res, next) {
  try {
    const { originalUrl, ip, method } = req;
    const { authorization = '' } = req.headers;

    if (method === 'OPTIONS' || EXCLUDE.includes(originalUrl.replace(/\?.*/, ''))) {
      next();
      return;
    }
    let passport;
    try {
      const data = jwt.verify(authorization.replace('Bearer ', ''), JWT_SECRET)
      passport = data.passport;

    } catch (e) {
      //
    }

    if (!passport) {
      throw HttpErrors(401);
    }
    req.passport = passport;
    next();
  } catch (e) {
    next(e);
  }
}
