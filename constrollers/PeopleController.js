import PeopleModel from "../models/PeopleModel";
import HttpErrors from "http-errors";
import jwt from "jsonwebtoken";
import People from "../models/People";

const { JWT_SECRET } = process.env

class PeopleController {

  static check = async (req, res, next) => {
    try {
      const { passport } = req.body;

      // const exists = await PeopleModel.getPeopleByPassport(passport);

      const user = await People.findAll({
        logging: console.log,
        order: [['fName', 'desc'], ['lName', 'desc']]
      });

      user[0].passport = '123123213';
      await user[0].save();
      console.log(11111, user[0].passport, user[0].getDataValue('passport'))

      // user.fName = 'AAAAAAAA';
      // await user.save();

      // await People.destroy({
      //   where: { id: 1 },
      // })
      // await People.create({
      //   fName: 'test',
      //   lName: 'aaa',
      //   mName: 'aaa',
      //   passport: 'aaa',
      //   birth: new Date(),
      // });


      res.json({
        user,
      });
      return;

      if (!exists) {
        throw  HttpErrors(404)
      }
      if (exists.voted) {
        throw HttpErrors(403)
      }
      const token = jwt.sign({
        passport
      }, JWT_SECRET)

      res.json({
        token: token
      })
    } catch (e) {
      next(e)
    }
  }
  static list = async (req, res, next) => {
    try {
      const { authorization } = req.headers;
      const data = jwt.verify(authorization.replace('Bearer ', ''), JWT_SECRET)
      if (!data) {
        HttpErrors(404)
      }
      const candidates = await People.getCandidates()
      for (let i in candidates) {
        const people = await People.getPeopleById(candidates[i].peopleId)
        candidates[i].people = people
      }
      res.json({
        status: 'ok',
        candidates
      })
    } catch (e) {
      next(e)
    }

  }
  static vote = async (req, res, next) => {
    try {
      const { passport } = req;
      const { id, } = req.body;

      const person = await People.getPeopleByPassport(passport)
      if (person.voted !== null) {
        throw  HttpErrors(403, 'Person already voted')
      }
      await People.personVote(passport)
      const data = await People.voteCandidate(id)
      res.json({
        data
      })
    } catch (e) {
      next(e)
    }
  }
}

export default PeopleController
