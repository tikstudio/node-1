import { Groups, Messages, Users, UsersGroupRelation } from "../models";
import Sequelize from 'sequelize'
import db from "../services/db";
import fs from 'fs';
import path from "path";
import sharp from "sharp";

class MessagesController {
  static list = async (req, res, next) => {
    try {
      const friendId = 2;
      const userId = 1;

      const messages = await Messages.findAll({
        logging: console.log,
        where: {},
        include: [{
          model: Groups,
          as: 'groups',
          include: [{
            model: Users,
            through: {
              model: UsersGroupRelation,
              attributes: [],
            },
            as: 'users',
          }]
        }],
        subQuery: false,
      });

      // const user = await Users.findOne({
      //   where: {
      //     id: userId,
      //   },
      //   include: [{
      //     model: Messages,
      //     as: 'messagesFrom'
      //   }]
      // })

      res.json({
        status: 'ok',
        // user,
        messages
      })

    } catch (e) {
      next(e)
    }
  }

  static create = async (req, res, next) => {
    try {
      const { file } = req;

      await sharp(file.buffer)
        .rotate()
        .resize(null, 512)
        .toFile(path.join(__dirname, '../public', 'test.jpg'))


      // fs.renameSync(file.path, path.join(__dirname, '../public', file.filename))
      // fs.writeFileSync(path.join(__dirname, '../public', file.originalname), file.buffer)

      // const friendId = 2;
      // const userId = 1;
      // const group = await Groups.findOne({
      //   where: {
      //     type: 'private'
      //   },
      //   include: [{
      //     model: UsersGroupRelation,
      //     required: true,
      //     as: 'relationFrom',
      //     where: {
      //       userId,
      //     }
      //   }, {
      //     model: UsersGroupRelation,
      //     required: true,
      //     as: 'relationTo',
      //     where: {
      //       userId: friendId,
      //     }
      //   }],
      //   subQuery: false,
      // })
      console.log(req.file)
      res.json({
        status: 'ok',
        body: req.body,
        file: req.file,
        files: req.files,
      });

    } catch (e) {
      // if (req.file && req.file.path) {
      //   fs.unlinkSync(req.file.path)
      // }
      next(e)
    }
  }
}

export default MessagesController;


