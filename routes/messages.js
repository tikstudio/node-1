import express from "express";
import MessagesController from "../constrollers/MessagesController";
import multer from "multer";
import imageUploader from "../middlewares/imageUploader";

const router = express.Router();


router.get('/', MessagesController.list);
router.get('/new', MessagesController.list);

router.post('/create', imageUploader.single('avatar'), MessagesController.create);

export default router
