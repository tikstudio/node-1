import express from "express";
import messages from './messages';

const router = express.Router();

router.use('/messages', messages)
//
// router.post('/people/check', PeopleController.check);
// router.get('/candidates/list', PeopleController.list);
// router.post('/candidates/vote',PeopleController.vote)

export default router
