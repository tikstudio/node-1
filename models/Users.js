import { DataTypes, Model } from "sequelize";
import db from "../services/db";
import md5 from "md5";

class Users extends Model {

}

Users.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  fName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  lName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
    set(val) {
      if (val) {
        this.setDataValue('password', md5(val, process.env.PASSWORD_SECRET))
      }
    },
    get() {
      return undefined;
    }
  },
  birth: {
    type: DataTypes.DATEONLY,
    allowNull: true,
  }
}, {
  tableName: 'users',
  modelName: 'Users',
  sequelize: db
})


export default Users;
