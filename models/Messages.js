import { DataTypes, Model } from "sequelize";
import db from "../services/db";
import Groups from "./Groups";
import Users from "./Users";

class Messages extends Model {

}

Messages.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  data: {
    type: DataTypes.TEXT('long'),
  },
}, {
  modelName: 'Messages',
  tableName: 'messages',
  sequelize: db
});

Messages.belongsTo(Groups, {
  as: 'groups',
  foreignKey: 'groupId',
  onUpdate: 'cascade',
  onDelete: 'cascade',
});
Groups.hasMany(Messages, {
  as: 'messages',
  foreignKey: 'groupId',
  onUpdate: 'cascade',
  onDelete: 'cascade',
});


Messages.belongsTo(Users, {
  as: 'userFrom',
  foreignKey: 'from',
  onUpdate: 'cascade',
  onDelete: 'cascade',
});

Users.hasMany(Messages, {
  as: 'messagesFrom',
  foreignKey: 'from',
  onUpdate: 'cascade',
  onDelete: 'cascade',
});
//
// Messages.belongsTo(Users, {
//   as: 'userTo',
//   foreignKey: 'to',
//   onUpdate: 'cascade',
//   onDelete: 'cascade',
// });
//
// Users.hasMany(Messages, {
//   as: 'messagesTo',
//   foreignKey: 'to',
//   onUpdate: 'cascade',
//   onDelete: 'cascade',
// });

export default Messages;
