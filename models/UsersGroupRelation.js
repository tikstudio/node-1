import { DataTypes, Model } from "sequelize";
import db from "../services/db";
import Groups from "./Groups";
import Users from "./Users";

class UsersGroupRelation extends Model {

}

UsersGroupRelation.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
}, {
  modelName: 'messageGroupRelation',
  tableName: 'messageGroupRelation',
  sequelize: db
});

Users.belongsToMany(Groups, {
  through: UsersGroupRelation,
  as: 'groups',
  foreignKey: 'userId',
});

Groups.belongsToMany(Users, {
  through: UsersGroupRelation,
  as: 'users',
  foreignKey: 'groupId',
});

Groups.hasMany(UsersGroupRelation, {
  foreignKey: 'groupId',
  as: 'relationFrom'
});
Groups.hasMany(UsersGroupRelation, {
  foreignKey: 'groupId',
  as: 'relationTo'
});

export default UsersGroupRelation;
