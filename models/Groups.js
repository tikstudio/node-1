import { DataTypes, Model } from "sequelize";
import db from "../services/db";

class Groups extends Model {

}

Groups.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  type: {
    type: DataTypes.ENUM(['private', 'group']),
    default: 'private',
    allowNull: false,
  },
  logo: {
    type: DataTypes.STRING,
    allowNull: true,
  }
}, {
  modelName: 'Groups',
  tableName: 'groups',
  sequelize: db
});

export default Groups;
